<?php
declare (strict_types = 1);

namespace qm\console\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Db;
use think\helper\Arr;
use think\helper\Str;

class Model extends Command
{
    /**
     * 要生成模型的表字段信息
     *
     * @var array
     * @author 秋月 414111907@qq.com
     */
    private $fields = [];
    /**
     * 传入的表名 也是生成的模型名称
     * @var string
     * @author 秋月 414111907@qq.com
     */
    private $name;
    /**
     * 模型后缀
     *
     * @var string|null
     * @author 秋月 414111907@qq.com
     */
    private $suffix;
    /**
     * 传入的模型命名空间
     * 决定最后生成的模型文件所在的位置
     * @var string
     * @author 秋月 414111907@qq.com
     */
    private $space;
    /**
     * 操作的表名
     * 表全名 包括前缀
     * @var string
     * @author 秋月 414111907@qq.com
     */
    private $tablename;
    /**
     * 表信息
     *
     * @var array
     * @author 秋月 414111907@qq.com
     */
    private $table;
    /**
     * 最终生成的模型文件地址
     *
     * @var string
     * @author 秋月 414111907@qq.com
     */
    private $file;

    protected function configure()
    {
        // 指令配置
        $this->setName('qmq:model')
            ->addArgument('name', Argument::REQUIRED, '模型名称')
            ->addUsage('生成模型 获取数据库字段与备注')
            ->addArgument('space', Argument::OPTIONAL, '命名空间[\\\分隔 app\\\admin]', 'app')
            ->addOption("suffix", "s", Option::VALUE_OPTIONAL, '模型后缀 false代表没有后缀', "Model")
            ->setDescription('创建模型');
    }

    protected function execute(Input $input, Output $output)
    {
        $name = $input->getArgument('name');
        $space = $input->getArgument('space');
        $suffix = $input->getOption("suffix");
        if ($suffix != "false") {
            $this->suffix = $suffix;
        }

        $this->name = Str::studly(trim($name));
        $this->space = $space;

        // 如果当前模型文件已存在，对当前文件进行备份
        $this->backUpFile();

        // 指令输出
        $content = $this->getFields()->template();

        file_put_contents($this->file, $content);
        $output->writeln("模型已生成" . $this->file);
    }

    /**
     * 获取数据表的字段信息
     *
     * @return $this
     * @author 秋月 414111907@qq.com
     */
    public function getFields()
    {
        if (count($this->fields)) {
            return $this;
        }
        $db = Db::name($this->name);
        $this->tablename = $db->getTable();
        $this->fields = $db->getFields();
        $result = $db->query("SELECT * FROM information_schema.`TABLES` WHERE TABLE_SCHEMA=:db AND TABLE_NAME=:tablename", ['db' => $db->getConfig('database'), 'tablename' => $this->tablename]);
        $this->table = array_change_key_case(Arr::first($result));

        return $this;
    }

    /**
     * 模板替换
     *
     * @return string
     * @author 秋月 414111907@qq.com
     */
    public function template()
    {
        $modelTemp = file_get_contents(__DIR__ . "/template/model.tpl");
        $fieldTemp = file_get_contents(__DIR__ . "/template/field.tpl");
        $this->fields = array_map(function ($item) {
            $item['date_type'] = preg_replace(['/.*int.*/', '/.*char.*/', '/text/'], ['int', 'string', 'string'], $item['type']);
            return $item;
        }, $this->fields);
        $fields = [];
        foreach ($this->fields as $key => $item) {
            if (!$item['default']) {
                $itemFieldTemp = str_replace(["默认值", ":default"], "", $fieldTemp);
            } else {
                $itemFieldTemp = $fieldTemp;
            }
            $fields[] = $this->bindParams($itemFieldTemp, $item);
        }

        $str = $this->bindParams($modelTemp, [
            'space' => $this->space,
            'class_name' => $this->name . $this->suffix,
            'name' => Str::snake($this->name),
            'tablename' => $this->tablename,
            'table_comment' => $this->table['table_comment'],
            'create_time' => $this->table['create_time'],
            'model_time' => date("Y-m-d H:i:s"),
            'fields' => $fields,
        ]);
        return $str;
    }

    /**
     * 获取文件名称 并返回当前文件是否已存在
     *
     * @return void
     * @author 秋月 414111907@qq.com
     */
    public function backUpFile()
    {
        $dir = app_path() . "../" . str_replace("\\", "/", $this->space) . "/model/";
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        $file = $dir . $this->name . $this->suffix . ".php";
        $this->file = $file;
        if (is_file($file)) {
            $new = $dir . $this->name . $this->suffix . date("YmdHis") . ".php";
            rename($file, $new);
        }

    }

    /**
     * 参数绑定 模板参数替换
     *
     * @param string $str
     * @param array $bind
     * @return string
     * @author 秋月 414111907@qq.com
     */
    protected function bindParams(string $str, array $bind = []): string
    {
        foreach ($bind as $key => $value) {
            if (is_array($value)) {
                $value = implode("\n", $value);
            }
            $str = str_replace(':' . $key, $value, $str);
        }
        return $str;
    }

}

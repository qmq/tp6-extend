<?php

namespace :space\model;


/**
 * 这个模型类来自表 :tablename
 *
 * :table_comment :create_time
 * 模型生成时间 :model_time
 *
:fields
 */
class :class_name extends Base{
    protected $name=":name";

    /**
     * 模型字段相关信息
     *
     * @param bool $selection 列表是否显示多选框
     *
     * @return array
     * @author 秋月 414111907@qq.com
     */
    public function getColumns()
    {
        $base = parent::getColumns();
        $columns = [];
        return array_merge_recursive($base, $columns);
    }
}
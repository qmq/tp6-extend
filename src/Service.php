<?php
namespace qm;

use qm\console\command\Model;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(Model::class);
    }
}
